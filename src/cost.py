# Objective Functions

import tensorflow as tf

class CrossEntropyLoss(BaseLoss):

  def calculate_loss(self, predictions, labels):
      epsilon = 10e-6
      float_labels = tf.cast(labels, tf.float32)
      cross_entropy_loss = float_labels * tf.log(predictions + epsilon) + 
                  (1 - float_labels) * tf.log(1 - predictions + epsilon)
      cross_entropy_loss = tf.negative(cross_entropy_loss)
      return tf.reduce_mean(tf.reduce_sum(cross_entropy_loss, 1))


class SoftmaxLoss(BaseLoss):

  def calculate_loss(self, predictions, labels):
      epsilon = 10e-8
      float_labels = tf.cast(labels, tf.float32)
      
      label_rowsum = tf.maximum(
          tf.reduce_sum(float_labels, 1, keep_dims=True), epsilon)
      norm_float_labels = tf.div(float_labels, label_rowsum)
      softmax_outputs = tf.nn.softmax(predictions)
      softmax_loss = tf.negative(tf.reduce_sum( tf.multiply(norm_float_labels, 
                        tf.log(softmax_outputs)), 1))
    return tf.reduce_mean(softmax_loss)